module ftpserver

go 1.14

require (
	github.com/goftp/file-driver v0.0.0-20180502053751-5d604a0fc0c9
	github.com/goftp/server v0.0.0-20190712054601-1149070ae46b
	github.com/jlaffaye/ftp v0.0.0-20200422224957-b9f3ade29122
)
