package ftpserver

import (
	"flag"
	"log"
	"os"
	"path/filepath"
	"strings"

	filedriver "github.com/goftp/file-driver"
	"github.com/goftp/server"
)

var (
	root = flag.String("root", getCurrentDirectory(), "Root directory to serve")
	user = flag.String("user", "admin", "Username for login")
	pass = flag.String("pass", "123456", "Password for login")
	port = flag.Int("port", 21, "Port")
	host = flag.String("host", "0.0.0.0", "Port")
)

func init() {

	flag.Parse()
}
func Server() {

	if *root == "" {
		*root = getCurrentDirectory()
	}
	factory := &filedriver.FileDriverFactory{
		RootPath: *root,
		Perm:     server.NewSimplePerm("user", "group"),
	}
	opts := &server.ServerOpts{
		Factory:  factory,
		Port:     *port,
		Hostname: *host,
		Auth:     &server.SimpleAuth{Name: *user, Password: *pass},
	}

	ftpServer := server.NewServer(opts)
	err := ftpServer.ListenAndServe()
	if err != nil {
		log.Fatal("Error starting server:", err)
	}
}
func getCurrentDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}
	return strings.Replace(dir, "\\", "/", -1)
}
