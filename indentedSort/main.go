package main

import (
	"fmt"
	"sort"
	"strings"
)

var sourceStr = []string{
	"Nonmetals",
	"    Hydrogen",
	"    Carbon",
	"    Nitrogen",
	"    Oxygen",
	"Inner Transitionals",
	"    Lanthanides",
	"        Europium",
	"        Cerium",
	"    Actinides",
	"        Uranium",
	"        Plutonium",
	"        Curium",
	"Alkali Metals",
	"    Lithium",
	"    Sodium",
	"    Potassium",
}

type NodesData []*NodeData

type NodeData struct {
	Str      string
	Children NodesData
}

func (nsd NodesData) Len() int {
	return len(nsd)
}

func (nsd NodesData) Less(i, j int) bool {
	return nsd[i].Str < nsd[j].Str
}

func (nsd NodesData) Swap(i, j int) {
	nsd[i], nsd[j] = nsd[j], nsd[i]
}

func main() {
	fmt.Println("|      Original      |      Sorted      |")
	fmt.Println("|--------------------|------------------|")
	sortStrSlice := IndentedSort(sourceStr)
	for i := range sourceStr {
		fmt.Printf("|%-20s|%-20s| \n", sourceStr[i], sortStrSlice[i])
	}
}

func IndentedSort(strslice []string) []string {

	NodesData := NodesData{}

	indentSize := findIndentSize(strslice)

	NodesData = initNodesData(indentSize, strslice, NodesData)

	SortStrString := []string{}

	Sorted(NodesData, &SortStrString, 0, indentSize)

	return SortStrString
}

func findIndentSize(strslice []string) int {
	for _, str := range strslice {
		if len(str) > 0 && str[1] == ' ' {
			for i, c := range str[1:] {
				if c != ' ' {
					return i + 1
				}
			}
		}
	}
	return 0
}

func initNodesData(indentSize int, strslice []string, root NodesData) NodesData {

	for _, str := range strslice {
		i, level := 0, 1
		for strings.HasPrefix(str[i:], " ") {
			i += indentSize
			level++
		}

		root = addNoteData(level, str[i:], root)
	}
	return root

}

func addNoteData(level int, str string, set NodesData) NodesData {

	if level == 1 {
		set = append(set, &NodeData{str, make(NodesData, 0)})
	} else {
		set[len(set)-1].Children = addNoteData(level-1, str, set[len(set)-1].Children)
	}

	return set

}

func Sorted(nodesData NodesData, SortStrString *[]string, level int, indentSize int) {

	sort.Sort(nodesData)

	for _, nodeData := range nodesData {

		*SortStrString = append(*SortStrString, strings.Repeat(" ", indentSize*level)+nodeData.Str)

		Sorted(nodeData.Children, SortStrString, level+1, indentSize)

	}
}
