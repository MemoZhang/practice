package sourceweb

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type ApiServerHandle interface {
	AcheviveImg(*ReqData)
}

type BaseConf struct {
	Url    string
	ApiKey string
}

type ReqData struct {
	Err          error
	MovieName    string
	MovieYear    string
	SourceImg    string
	SaveDocument string
	SaveFileName string
}

func CheckParam(r *ReqData) {
	if len(r.MovieName) == 0 {
		r.Err = errors.New("Please input movie name.")
	}

	_, err := os.Stat(r.SaveDocument)

	if err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(r.SaveDocument, os.ModePerm)
			if err != nil {
				panic(err)
			}
		} else {
			panic(err)
		}
	}
}

func httpGetJson(url string, m *map[string]interface{}) error {

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = json.Unmarshal(body, &m)

	if err != nil {
		return err
	}

	return nil

}

func httpGetRaw(url string, raw []byte) error {
	resp, err := http.Get(url)

	if err != nil {
		return err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	raw = body

	return nil
}

func createFileName(saveDir, srcImg string) string {
	rand.Seed(time.Now().UnixNano())
	randFileName := rand.Uint64()
	randFileNameStr := strconv.FormatUint(randFileName, 10)

	extArr := strings.Split(srcImg, ".")
	ext := extArr[len(extArr)-1]

	fileName := saveDir + randFileNameStr + "." + ext
	return fileName
}
