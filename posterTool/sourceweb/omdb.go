package sourceweb

import (
	"errors"
	"os"
	"strings"
)

var (
	apiKey    = "fe407bea"
	searchUrl = "http://www.omdbapi.com/"
)

//可扩展其他服务
type OmdbConf struct {
	BaseConf
}

func NewOmdb() *OmdbConf {
	return &OmdbConf{
		BaseConf{
			Url:    searchUrl,
			ApiKey: apiKey,
		},
	}
}

func (conf *OmdbConf) AcheviveImg(r *ReqData) {
	conf.acheviveImgPath(r)
	if r.Err != nil {
		return
	}
	conf.downloadImg(r)
}

func (conf *OmdbConf) acheviveImgPath(r *ReqData) {

	url := conf.Url + "?t=" + r.MovieName + "&y=" + r.MovieYear + "&apikey=" + conf.ApiKey

	m := make(map[string]interface{})

	r.Err = httpGetJson(url, &m)
	if r.Err != nil {
		return
	}

	rst := m["Response"].(string)

	if strings.ToLower(rst) != "true" {
		errMsg := m["Error"].(string)
		r.Err = errors.New(errMsg)
		return
	}
	r.SourceImg = m["Poster"].(string)

}

func (conf *OmdbConf) downloadImg(r *ReqData) {

	imgCont := []byte{}
	r.Err = httpGetRaw(r.SourceImg, imgCont)
	if r.Err != nil {
		return
	}

	r.SaveFileName = createFileName(r.SaveDocument, r.SourceImg)

	newFileHandler, err := os.Create(r.SaveFileName)
	if err != nil {
		panic(err)
	}
	defer newFileHandler.Close()

	_, err = newFileHandler.Write(imgCont)

	if err != nil {
		panic(err)
	}

}
