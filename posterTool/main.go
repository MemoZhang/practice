package main

import (
	"1/sourceweb"
	"flag"
	"fmt"
	"os"
	"strings"
)

func main() {

	var serverHandler sourceweb.ApiServerHandle
	var movieName, movieYear, savePath, serverType string

	flag.StringVar(&movieName, "n", "", "Search movie name")
	flag.StringVar(&movieYear, "y", "", "Search movie year")
	flag.StringVar(&savePath, "d", "./", "Download image file document")
	flag.StringVar(&serverType, "t", "omdbapi", "Choose service provider name")

	flag.Parse()

	switch strings.ToLower(serverType) {

	case "omdbapi":
		serverHandler = sourceweb.NewOmdb()
	default:
		fmt.Printf("Not support service provider:%s \n", serverType)
		return

	}

	requestData := &sourceweb.ReqData{
		MovieName:    movieName,
		MovieYear:    movieYear,
		SaveDocument: savePath,
	}

	sourceweb.CheckParam(requestData)

	if requestData.Err != nil {
		gameOver(requestData.Err)
		return
	}

	serverHandler.AcheviveImg(requestData)

	if requestData.Err != nil {
		gameOver(requestData.Err)
		return
	}
}
func gameOver(err error) {
	fmt.Printf("%s \n", err.Error())
	os.Exit(1)
}
