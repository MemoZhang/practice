package main

import (
	"flag"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

const (
	_ = iota
	K = 1 << (10 * iota)
	M
	G
	T
)

func main() {

	var dirPath, fileName, fileSize string

	flag.StringVar(&dirPath, "d", "./", "Assign Dir Path")
	flag.StringVar(&fileName, "n", "", "Filter File Name")
	flag.StringVar(&fileSize, "s", "", "Filter File Size (K M G T)")

	flag.Parse()

	//judge filer model
	filterMode := filterMode(fileName, fileSize)

	//calculate filesize
	compareFileSize := calFileSize(fileSize)

	//file dir pipeline
	filterDir(dirPath, fileName, filterMode, compareFileSize)
}

func filterMode(filterName, filterSize string) string {

	var filterMode string

	if len(filterName) != 0 {
		filterMode = "Name"
	}

	return filterMode

}

func calFileSize(fileSize string) (compareFileSize int64) {

	compareFileSize = 1

	if len(fileSize) > 0 {

		sizeStr := fileSize[:len(fileSize)-1]
		size, err := strconv.ParseInt(sizeStr, 10, 64)
		if err != nil {
			panic(err)
		}

		compareFileSize *= size

		unit := strings.ToUpper(string(fileSize[len(fileSize)-1]))
		switch unit {
		case "K":
			compareFileSize *= K
		case "M":
			compareFileSize *= M

		case "G":
			compareFileSize *= G

		case "T":
			compareFileSize *= T

		}
	}
	return
}

func filterDir(dirPath, fileName, filterMode string, compareFileSize int64) {

	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		panic(err)
	}

	for _, v := range files {

		switch filterMode {

		case "Name":
			findRst := strings.HasSuffix(v.Name(), fileName)
			if findRst && compareFileSize <= v.Size() {
				io.WriteString(os.Stdout, v.Name()+"\n")
			}

		default:
			if compareFileSize <= v.Size() {
				io.WriteString(os.Stdout, v.Name()+"\n")
			}
		}

	}
}
