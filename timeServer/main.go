package main

import (
	"2/timeserver"
	"fmt"
)

var timeConf = timeserver.TimeConf

func main() {

	fmt.Printf("Time Server is beginning at: %s \n", timeConf.ServerIPAndPort)
	timeConf.Listen()
}
