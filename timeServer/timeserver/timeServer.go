package timeserver

import (
	"flag"
	"io"
	"log"
	"net"
	"time"

	"gopkg.in/ini.v1"
)

var TimeConf *timeConf

func init() {
	TimeConf = &timeConf{}

	var confPath string

	flag.StringVar(&confPath, "conf", "./conf/conf.ini", "Start Server Configuration")

	flag.Parse()
	conf, err := ini.Load(confPath)
	if err != nil {
		panic(err)
	}

	err = conf.Section("ServerConf").MapTo(TimeConf)
	if err != nil {
		panic(err)
	}

}

type timeConf struct {
	ServerIPAndPort string
	SendInterval    int
	Barrier         string
}

func (tf *timeConf) Listen() {

	listener, err := net.Listen("tcp", tf.ServerIPAndPort)

	if err != nil {
		panic(err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go tf.handleConn(conn)
	}

}

func (tf *timeConf) handleConn(conn net.Conn) {

	defer conn.Close()

	c := time.Tick(time.Duration(tf.SendInterval) * time.Second)

	for {
		select {

		case <-c:
			_, err := io.WriteString(conn, time.Now().Format("2006-01-02 15:04:05"+tf.Barrier))
			if err != nil {
				log.Println(err)
				return
			}
		default:
		}

	}

}
